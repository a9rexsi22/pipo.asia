from utan.settings.base import *

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.1.7']
DEBUG = True
MEDIA_URL = '/media/'

GLOBAL_MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES
LOCAL_MIDDLEWARE_CLASSES = ('debug_toolbar.middleware.DebugToolbarMiddleware',)
MIDDLEWARE_CLASSES = GLOBAL_MIDDLEWARE_CLASSES + LOCAL_MIDDLEWARE_CLASSES



def show_toolbar(request):
    return True
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
}

INSTALLED_APPS += (
   'debug_toolbar', 
)