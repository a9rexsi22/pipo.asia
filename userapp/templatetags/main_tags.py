from django import template
from django.contrib.auth.models import User
from userapp.models import UserProfile

import urlparse
register = template.Library()


def getname(id):
    getuser = UserProfile.objects.only("full_name").get(user_id=id)
    return getuser.full_name

#def getname(id):
    #getuser = UserProfile.objects.only("full_name").get(user_id=id)
    #return getuser.full_name

register.filter('getname', getname)


