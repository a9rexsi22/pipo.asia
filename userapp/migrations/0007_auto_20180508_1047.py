# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0006_auto_20180414_0445'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='get_inves',
            field=models.PositiveIntegerField(default=0, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='get_profit',
            field=models.PositiveIntegerField(default=0, null=True, blank=True),
        ),
    ]
