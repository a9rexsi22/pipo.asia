# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0007_auto_20180508_1047'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='get_inves',
            new_name='total_invest',
        ),
        migrations.RenameField(
            model_name='userprofile',
            old_name='get_profit',
            new_name='total_profit',
        ),
    ]
