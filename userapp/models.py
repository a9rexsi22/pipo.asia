from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

class GetOrNoneManager(models.Manager):
    """Adds get_or_none method to objects
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    full_name = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    no_rek = models.DecimalField(blank=True, null=True, max_digits=30, decimal_places=0)
    email = models.CharField(max_length=100, blank=True, null=True)
    dob = models.DateField(blank=True, null=True) #date of birth
    ROLE = (('1', 'admin'), ('2', 'customer'))
    role = models.CharField(max_length=2, choices=ROLE)
    joined =  models.DateTimeField(auto_now_add=True)
    login = models.DateTimeField(blank=True, null=True)
    logout = models.DateTimeField(blank=True, null=True)
    total_invest =  models.PositiveIntegerField(blank=True, null=True, default=0)
    total_profit = models.PositiveIntegerField(blank=True, null=True, default=0)
    PHOTOS_DIR_NAME = "profilepicture"
    photo = models.ImageField('Photo', upload_to=PHOTOS_DIR_NAME, blank=True, null=True)
    
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    objects = GetOrNoneManager()
 

    def __unicode__(self):
        return self.full_name
    
admin.site.register(UserProfile)
       
class MyBalance(models.Model):
    user = models.ForeignKey(UserProfile,null=True)
    money = models.DecimalField(blank=True, null=True, max_digits=30, decimal_places=0)
    investing = models.DecimalField(blank=True, null=True, max_digits=30, decimal_places=0)
    
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    objects = GetOrNoneManager()                                     
    def __str__(self):
        return self.money
    
