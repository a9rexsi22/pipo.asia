from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from userapp.models import UserProfile
from django.template import defaultfilters
from decimal import Decimal
import uuid
import random
import string
import unicodedata

from easy_pdf.rendering import render_to_pdf
from xhtml2pdf import pisa 
import cStringIO as StringIO
import cgi

from django.http import HttpResponse
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage

class GetOrNoneManager(models.Manager):
    """Adds get_or_none method to objects
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None
        
        
def key_generator(limit=10):
    uuid_set = str(uuid.uuid4().fields[-1])[:5]
    d = [random.choice(string.letters + string.digits + uuid_set) for x in xrange(limit)]
    key = "".join(d)
    return key

def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html  = template.render(context)
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return result
    return None

def sendemail(subject, to, data, html):
    try:
        message = get_template(html).render(Context(data))
        pdf = render_to_pdf(html, data)
        msg = EmailMessage(subject, message, to=to, from_email="admin@pipo.asia")
        msg.content_subtype = 'html'
        msg.attach('invoicex.pdf', pdf.getvalue() , 'application/pdf')
        return msg.send()
    except Exception:
        return False

class Project(models.Model):
    name = models.CharField(max_length=255, unique=True)
    uniquecode = models.CharField(max_length=300, blank=True, null=True, unique=True)
    slug = models.CharField(max_length=300, blank=True, null=True, unique=True)
    price = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=0)
    lama_investasi = models.CharField(max_length=255, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    finding_needs = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=0)
    profit_prediction = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=0)
    risk_sts = (('1', 'Rendah'), ('2', 'Sedang'), ('3', 'Tinggi'))
    risk_status = models.CharField(max_length=1, blank=True, null=True, choices=risk_sts)
    lat = models.CharField(max_length=100, blank=True, null=True)
    lang = models.CharField("long", max_length=100, blank=True, null=True)
    lokasi  = models.TextField(blank=True, null=True, verbose_name='lokasi', help_text="Deskripsi lokasi") 
    description  = models.TextField(blank=True, null=True, verbose_name='deskripsi', help_text="Deskripsi produk organik blablabla") 
    photo = models.ImageField('Photo', upload_to="photo_proj", blank=True, null=True)
    active = models.BooleanField(default=True)
    sts_project = (('1', 'Menunggu Pendanaan Terpenuhi'), ('2', 'Proses Tanam'), ('3', 'Panen'))
    project_status = models.CharField(max_length=1, blank=True, null=True, choices=sts_project, default='1')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    aoi = models.DecimalField(blank=True, null=True, max_digits=18, decimal_places=0)
    totalmember = models.PositiveIntegerField(blank=True, null=True, default=0)
    objects = GetOrNoneManager()
             
    def get_persen(self):
        kebutuhan = Decimal(self.finding_needs)
        terkumpul = Decimal(self.aoi)
        persen = terkumpul*100/kebutuhan
        persen = round(persen)
        
        print terkumpul
        return format(persen, '.15g')  
            
    
    def save(self, *args, **kwargs):
        if not self.aoi:
            self.aoi = 0
        self.slug = defaultfilters.slugify(self.name)
        self.uniquecode = defaultfilters.slugify(key_generator(5))
        super(Project, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return self.name



class Member(models.Model):
    project = models.ForeignKey(Project, verbose_name='project',blank=True, null=True)
    user = models.ForeignKey(UserProfile, verbose_name='UserProfile', blank=True, null=True)
    uniquecode = models.CharField(max_length=300, blank=True, null=True, unique=True)
    aoi = models.DecimalField(blank=True, null=True, max_digits=18, decimal_places=0)
    profit = models.DecimalField(blank=True, null=True, max_digits=18, decimal_places=0)
    toi =models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
    objects = GetOrNoneManager()
    
    def save(self, *args, **kwargs):
        if self.status:
            transaction = Transaction.objects.create(
                member=Member.objects.get(id = self.id),
                moneyin=self.aoi,
            )
            
            transaction.save()
            #masukan aoi, totalmember, activeinvest, profitforecast
            
            self.user.total_invest += self.aoi
            self.user.total_profit += self.profit
            
            self.user.save()

        else: 
            subject = "Pipo.asia: Invoice #%s Untuk Proyek %s" % (self.uniquecode, self.project.name)
            to = [self.user.email]
            html = "invoice.html"
            data = {
               'invoiceid': self.project.uniquecode,
               'pname': self.project.name,
               'paoi': self.aoi
            }
            sendemail(subject, to, data, html)

        super(Member, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return unicode(self.uniquecode) or u''
     


#class untuk galery foto dari project
class Gallery(models.Model):
    project = models.ForeignKey(Project,blank=True, null=True)
    name = models.CharField(blank=True, null=True, max_length=200)
    slug = models.CharField(max_length=300, blank=True, null=True)
    photo = models.ImageField('Photo', upload_to="photo_proj", blank=True, null=True)

    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    
    def save(self, *args, **kwargs):
        self.name = self.photo.name
        self.slug = defaultfilters.slugify(self.name)
        super(Gallery, self).save(*args, **kwargs)

    def __str__(self):
        return "%s" % (self.name)

admin.site.register(Gallery)

class Transaction(models.Model):
    member = models.ForeignKey(Member, null=True)
    moneyout = models.DecimalField(max_digits=20, blank=True, null=True, decimal_places=0)
    moneyin = models.DecimalField(max_digits=20, blank=True, null=True, decimal_places=0)
    
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    
    def __unicode__(self):
        return unicode(self.id) or u''
    
admin.site.register(Transaction)


