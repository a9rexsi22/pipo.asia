# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View
from django.views.generic.base import TemplateView
from investasi.models import Project,Member
from userapp.models import UserProfile
from django.contrib.auth.models import User
import datetime
from decimal import Decimal
import uuid
import random
import string
import unicodedata
from datetime import datetime


def key_generator(limit=10):
    uuid_set = str(uuid.uuid4().fields[-1])[:5]
    d = [random.choice(string.letters + string.digits + uuid_set) for x in xrange(limit)]
    key = "".join(d)
    return key

class HomePageView(View):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        objects = Project.objects.only("aoi", "lokasi", "slug", "name", "finding_needs", "lama_investasi", "risk_status", "photo").all()[:4]
        context = dict(
            objects=objects
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        pass

class DetailProject(View):
    template_name = 'detail.html'

    def get(self, request, *args, **kwargs):
        slug = kwargs["slug"]
        objects = Project.objects.get_or_none(slug=slug)
        data_inves = []
        user = request.user
        if user.is_authenticated():
            try:
                data_inves = Member.objects.only("status", "uniquecode").get(user__user_id = user.id , project = objects)
            except Exception:
                pass

        context = dict(
            objects=objects,
            data_inves = data_inves
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        
        #data From Detail Project Template Detail.html
        jmlh = request.POST.get("jmlh")
        profit = request.POST.get("profit")
        key = request.POST.get("id")
        param = request.POST.get("param")
        csrf = request.POST.get("csrfmiddlewaretoken")
        try:
            #try insert for member 
            
            member, created = Member.objects.get_or_create(
                user=UserProfile.objects.get(user=request.user),
                project=Project.objects.get(id=key),
            )
            if not created and not param:
                return HttpResponse("Already")
            
            member.aoi = Decimal(jmlh) 
            member.profit = Decimal(profit) 
            member.uniquecode = "%04d" % (Decimal(key),)
            member.save()
        except Exception:
            return HttpResponse("false")
        return HttpResponse("success")
    
class Transaksi(View):
    template_name = 'transaksi.html'

    def get(self, request, *args, **kwargs):
        slug = kwargs["slug"]
        puser = UserProfile.objects.get(user = request.user)
        data_inves = Member.objects.get(uniquecode = slug)
        context = dict(
            objects=puser,
            data_inves=data_inves
            
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))
        
    def post(self, request, *args, **kwargs):
        pass
        
class Invoice(View):
    template_name = 'invoice.html'

    def get(self, request, *args, **kwargs):
        puser = UserProfile.objects.get(user = request.user)
        context = dict(
            objects=puser,
            
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))
        
    def post(self, request, *args, **kwargs):
        pass
    
    
class UserView(View):
    template_name = "user_profile.html"
    
    def get(self, request, *args, **kwargs):
        puser = UserProfile.objects.select_related("user").get(user = request.user)
        data_inves = Member.objects.filter(user_id = puser.id).select_related("project")
        cek_data_inves = data_inves.filter(status=True).count()
        context = dict(
            profile = True,
            objects=puser,
            cek_data_inves = cek_data_inves,
            data_inves=data_inves
        )
        return render_to_response(
           self.template_name,
           context,
           context_instance=RequestContext(request))
        
    def post(self, request, *args, **kwargs):
        
       
        #data From Detail Project Template Detail.html
        full_name = request.POST.get("full_name")
        address = request.POST.get("address")
        email = request.POST.get("email")
        phone = request.POST.get("phone")
        dob = request.POST.get("dob")
        
        csrf = request.POST.get("csrfmiddlewaretoken")
        
        try:
            profile, created = UserProfile.objects.get_or_create(
                user=User.objects.get(username=request.user.username)
            )
            profile.full_name = full_name
            profile.address = address
            profile.email = email
            profile.phone = phone
            profile.save()
            if dob:
                try:
                    dob = dob.split("-")
                    dob = datetime(int(dob[0]), int(dob[1]), int(dob[2]))
                    profile.dob = dob
                    profile.save()
                except Exception:
                    pass

        except Exception:
            return HttpResponse("gagalupdate")
        return HttpResponse("berhasilupdate")
    
    
    
       